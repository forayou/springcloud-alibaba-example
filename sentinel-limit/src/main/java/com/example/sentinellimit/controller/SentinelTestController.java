package com.example.sentinellimit.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author xiaojing
 */
@RestController
@RequestMapping("/sentinel")
public class SentinelTestController {


    @GetMapping(value = "/hello")
    //@SentinelResource("resource")
    public String hello() {
        return "Hello";
    }

    @GetMapping(value = "/hello2")
    @SentinelResource("resource")
    public String hello2() {
        return "Hello";
    }

}
