package com.example.nacosserver.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("nacos")
public class NacosServerController {

    @RequestMapping("/hello")
    public String nacosDiscovery() {
        return "hello nacos";
    }

}
