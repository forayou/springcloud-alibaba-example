package com.example.nacosconfig.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@RefreshScope
public class ExampleConfig {

    @Value("${user.name}")
    private String userName;

    @Value("${user.age:25}")
    private int age;

    /*@Value("${nacos.config}")
    private String config;*/
}
