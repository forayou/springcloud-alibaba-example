package com.example.nacosconfig.nacosconfig;

import com.example.nacosconfig.config.ExampleConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author <a href="mailto:chenxilzx1@gmail.com">theonefx</a>
 */
@RestController
@RefreshScope
public class SampleController {

    @Value("${user.name}")
    String userName;

    @Value("${user.age:25}")
    int age;

    //@Value("${nacos.config}")
    String nacosConfig;

    @Resource
    private ExampleConfig exampleConfig;


    @RequestMapping("/user")
    public String simple() {
        return "Hello Nacos Config!" + "Hello " + userName + " " + age + "!";
    }

    @RequestMapping("/config")
    public String nacosConfig() {
        return "Hello Nacos Config!" + "Hello " + nacosConfig + "!";
    }
}
