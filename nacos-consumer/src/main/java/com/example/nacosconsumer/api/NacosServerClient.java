package com.example.nacosconsumer.api;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient("nacos-server")
public interface NacosServerClient {

    @RequestMapping("/nacos/hello")
    String nacosHello();
}
