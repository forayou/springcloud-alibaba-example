package com.example.nacosconsumer.controller;

import com.example.nacosconsumer.api.NacosServerClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

@Slf4j
@RestController
@RequestMapping("nacos-consumer")
public class NacosConsumerController {

    @Resource
    private LoadBalancerClient loadBalancerClient;

    @Resource
    private NacosServerClient nacosServerClient;

    @Resource
    private RestTemplate restTemplate;

    @RequestMapping("/consumer")
    public String nacosConsumer() {

        // 通过spring cloud common中的负载均衡接口选取服务提供节点实现接口调用
        ServiceInstance serviceInstance = loadBalancerClient.choose("nacos-server");
        String url = serviceInstance.getUri() + "/nacos-server/nacos";
        RestTemplate restTemplate = new RestTemplate();
        String result = restTemplate.getForObject(url, String.class);
        return "Invoke : " + url + ", return : " + result;
    }

    /**
     * feignClient 方式调用
     * @return
     */
    @RequestMapping("/feignConsumer")
    public String nacosFeignConsumer() {
        return nacosServerClient.nacosHello();
    }


    /**
     * restTemplate + Ribbon 方式调用
     * @return
     */
    @RequestMapping("/nacosRestConsumer")
    public String nacosRestConsumer() {
        return restTemplate.getForObject("http://nacos-server/hello/nacos", String.class);
    }


}
